package com.adpremium.adsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdsecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdsecurityApplication.class, args);
	}

}
